package com.example.tic_tac_toebygiorgimatcharashvili

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_open_menu.*

class OpenMenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_menu)
        jumpToGameActivity()
    }
    private fun jumpToGameActivity() {
        startGameButton.setOnClickListener {
            val startIntent = Intent(this, GameActivity::class.java)
            startActivity(startIntent)
        }
    }
}