package com.example.tic_tac_toebygiorgimatcharashvili

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_game.*

class GameActivity : AppCompatActivity(), View.OnClickListener {
    private var isPlayerOne = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        init()
    }
    private fun init(){
        Button00.setOnClickListener(this)
        Button01.setOnClickListener(this)
        Button02.setOnClickListener(this)
        Button10.setOnClickListener(this)
        Button11.setOnClickListener(this)
        Button12.setOnClickListener(this)
        Button20.setOnClickListener(this)
        Button21.setOnClickListener(this)
        Button22.setOnClickListener(this)
    }

    private fun buttonChange(button: Button){
        if(isPlayerOne){
            button.text = "X"
        } else{
            button.text = "0"
        }
        button.isClickable = false
        isPlayerOne = !isPlayerOne
        checkWinner()
    }
    private fun checkWinner(){
        if(Button00.text.toString().isNotEmpty() && Button00.text.toString()==Button01.text.toString() && Button01.text.toString()==Button02.text.toString()){
            show(Button00.text.toString())
        }else if(Button10.text.toString().isNotEmpty() && Button10.text.toString()==Button11.text.toString() && Button11.text.toString()==Button12.text.toString()){
            show(Button10.text.toString())
        }else if(Button20.text.toString().isNotEmpty() && Button20.text.toString()==Button21.text.toString() && Button21.text.toString()==Button22.text.toString()){
            show(Button20.text.toString())
        }else if(Button00.text.toString().isNotEmpty() && Button00.text.toString()==Button10.text.toString() && Button10.text.toString()==Button20.text.toString()){
            show(Button00.text.toString())
        }else if(Button01.text.toString().isNotEmpty() && Button01.text.toString()==Button11.text.toString() && Button11.text.toString()==Button21.text.toString()){
            show(Button01.text.toString())
        }else if(Button02.text.toString().isNotEmpty() && Button02.text.toString()==Button12.text.toString() && Button12.text.toString()==Button22.text.toString()){
            show(Button02.text.toString())
        }else if(Button00.text.toString().isNotEmpty() && Button00.text.toString()==Button11.text.toString() && Button11.text.toString()==Button22.text.toString()){
            show(Button00.text.toString())
        }else if(Button02.text.toString().isNotEmpty() && Button02.text.toString()==Button11.text.toString() && Button11.text.toString()==Button20.text.toString()){
            show(Button02.text.toString())
        }else if(Button00.text.toString().isNotEmpty() && Button01.text.toString().isNotEmpty() && Button02.text.toString().isNotEmpty() && Button10.text.toString().isNotEmpty() && Button11.text.toString().isNotEmpty() && Button12.text.toString().isNotEmpty() && Button20.text.toString().isNotEmpty()&& Button21.text.toString().isNotEmpty() && Button22.text.toString().isNotEmpty()){
            show("")
        }
    }

    private fun show(message: String){
        val startNew = Intent(this, ResetActivity::class.java)
        startNew.putExtra("Winner", message)
        startActivity(startNew)

    }


    override fun onClick(v: View?) {
        buttonChange(v as Button)
    }
}