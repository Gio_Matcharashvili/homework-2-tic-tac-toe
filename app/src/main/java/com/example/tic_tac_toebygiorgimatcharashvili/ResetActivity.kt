package com.example.tic_tac_toebygiorgimatcharashvili

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_reset.*

class ResetActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset)
        init()
    }
    private fun init(){
        var sms = intent.getStringExtra("Winner")
        if (sms == ""){
            sms = "Draw"
        } else {
            sms = "Winner is $sms"
        }
        showWinnerTextView.text = sms
        resetButton.setOnClickListener{
            val reset = Intent(this, GameActivity::class.java)
            startActivity(reset)
        }
    }
}